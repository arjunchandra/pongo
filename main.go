package main

import (
	"encoding/json"
	"log"
	"math"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

type Table struct {
	sync.Mutex
	Left, Right, BallX, BallY float64
	ballDX, ballDY            float64
}

func (t *Table) reset() {
	t.Left = tableHeight / 2
	t.Right = tableHeight / 2
	t.BallX = tableWidth / 2
	t.BallY = tableHeight / 2
	angle := 2 * math.Pi * rand.Float64()
	t.ballDX = 5 * math.Cos(angle)
	t.ballDY = 5 * math.Sin(angle)
}

var table Table

func getTableState(w http.ResponseWriter, req *http.Request) {
	table.Lock()
	b, err := json.Marshal(table)
	table.Unlock()
	if err != nil {
		log.Fatal("getTableState: ", err)
	}
	w.Write(b)
}

func main() {
	table.reset()
	go animate()
	http.Handle("/", http.FileServer(http.Dir("")))
	http.HandleFunc("/table", getTableState)
	// http.HandleFunc("/move", setTableState)
	err := http.ListenAndServe(":12345", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

const (
	fps         = 30
	tableWidth  = 16
	tableHeight = 10
	paddleWidth = .5
	ballRadius  = .3
)

func animate() {
	for {
		table.Lock()
		table.BallX += table.ballDX / fps
		table.BallY += table.ballDY / fps
		if table.BallY < ballRadius || table.BallY > tableHeight-ballRadius {
			table.ballDY = -table.ballDY
		}
		if table.BallX < paddleWidth+ballRadius && table.BallY > table.Left-1 && table.BallY < table.Left+1 ||
			table.BallX > tableWidth-(paddleWidth+ballRadius) && table.BallY > table.Right-1 && table.BallY < table.Right+1 {
			table.ballDX = -table.ballDX
		}
		if table.BallX < 0 || table.BallX > tableWidth {
			table.reset()
		}
		table.Unlock()

		time.Sleep(time.Second / fps)
	}
}
