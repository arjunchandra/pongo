//+build js

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gopherjs/jquery"
)

type Table struct {
	Left, Right, BallX, BallY float64
}

var (
	jQuery = jquery.NewJQuery

	left  = jQuery("line#left")
	right = jQuery("line#right")
	ball  = jQuery("circle#ball")
	table Table
)

const fps = 30

func main() {
	for {
		resp, err := http.Get("http://localhost:12345/table")
		if err != nil {
			panic(err)
		}

		tableBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		err = json.Unmarshal(tableBytes, &table)
		if err != nil {
			panic(err)
		}

		left.SetAttr("y1", fmt.Sprintf("%vcm", table.Left-1))
		left.SetAttr("y2", fmt.Sprintf("%vcm", table.Left+1))
		right.SetAttr("y1", fmt.Sprintf("%vcm", table.Right-1))
		right.SetAttr("y2", fmt.Sprintf("%vcm", table.Right+1))
		ball.SetAttr("cx", fmt.Sprintf("%vcm", table.BallX))
		ball.SetAttr("cy", fmt.Sprintf("%vcm", table.BallY))
		time.Sleep(time.Second / fps)
	}
}
